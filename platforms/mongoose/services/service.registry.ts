import { Mongoose } from "mongoose";
import { WalletService } from "./wallet.service";

export class ServiceRegistry {

    readonly walletService : WalletService;
    
    constructor(connection: Mongoose) {
        this.walletService = new WalletService(connection);
    }
}