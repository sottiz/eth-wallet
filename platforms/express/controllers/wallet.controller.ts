import { Router, Request, Response } from "express";
import { ServiceRegistry } from "../../mongoose";
import { authMiddleware } from "@sottiz/eth-server";
import { ValidationUtils } from "@sottiz/eth-lib";
import { WalletCreateDTO } from "../../../definitions/wallets/wallet.create.dto";
import { WalletUpdateDTO } from "../../../definitions/wallets";

export class WalletController {

    constructor(readonly serviceRegistry: ServiceRegistry) {
    }
    
    async getWallets(req: Request, res: Response) {
        const account = req.session.account;
        const wallets = await this.serviceRegistry.walletService.getAll(account);
        res.json(wallets);
    }

    async createWallet(req: Request, res: Response) {
        const dto = await ValidationUtils.createAndValidateDTO(WalletCreateDTO, req.body, res);
        const account = req.session.account["_id"];
        const wallet = await this.serviceRegistry.walletService.create(dto, account);
        res.json(wallet);
    }

    async updateWallet(req: Request, res: Response) {
        // check if user is the wallet owner
        const dto = await ValidationUtils.createAndValidateDTO(WalletUpdateDTO, req.body, res);
        const walletId = req.params.id;
        const account = req.session.account;
        const wallet = await this.serviceRegistry.walletService.update(dto, walletId, account);
        if(wallet) {
            res.json(wallet);
        } else {
            res.status(403).end();
        }
    }

    async deleteWallet(req: Request, res: Response) {
        const walletId = req.params.id;
        const account = req.session.account;
        const wallet = await this.serviceRegistry.walletService.deleteWallet(walletId, account);
        if(wallet) {
            res.status(204).end();
        } else {
            res.status(403).end();
        }
    }

    buildRoutes(): Router {
        const router = Router();
        router.delete("/:id", authMiddleware(), this.deleteWallet.bind(this));
        router.patch('/:id', authMiddleware(), this.updateWallet.bind(this));
        router.get('/', authMiddleware(), this.getWallets.bind(this));
        router.post('/', authMiddleware(), this.createWallet.bind(this));
        return router;
    }
}