import * as express from 'express';
import { ServiceRegistry } from '../mongoose';
import { WalletController } from './controllers';

export function startServer(serviceRegistry: ServiceRegistry): void {
  const app = express();
  app.use(express.json());
  
  const walletController = new WalletController(serviceRegistry);
  app.use('/wallet', walletController.buildRoutes());
  

  const port = process.env.PORT;
  
  app.listen(port, () => {
    console.log(`Listening on port ${port}...`);
  });
}