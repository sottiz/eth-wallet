import 'reflect-metadata';
import { config } from 'dotenv';
config();

import { startServer } from './platforms/express';
import { MongooseUtils } from '@sottiz/eth-lib';
import { ServiceRegistry } from './platforms/mongoose';

async function main() {
  
  const connection = await MongooseUtils.connect();

  startServer(new ServiceRegistry(connection));

}

main().catch(console.error);
